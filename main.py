import calculator


def main():
    # Boolean for running calculator
    quit_calc = False

    # User Menu
    print(
        "Hello and welcome to the calculator\n"
        "Alternatives: \n"
        "q - Quit Calculator\n"
        "r - Restart Calculator\n"
        "+ - Add \n"
        "- - Subtract \n"
        "/ - Divide \n"
        "* - Multiply \n"
    )

    # Initial User input number
    # If not a given number. Quit Calculator.
    calculated_sum = input("Start Calculator by entering a number: ")
    if not calculator.is_float(number=calculated_sum) or not calculator.is_int(
        number=calculated_sum
    ):
        print("Failed to start Calculator. Shutting down!")
        quit_calc = True

    # Run Calculator
    while not quit_calc:
        restart_calc = False

        # User select operation
        operation = input("Select operation: ")

        # Check if valid operation
        if (
            operation == "q"
            or operation == "r"
            or operation == "+"
            or operation == "-"
            or operation == "/"
            or operation == "*"
        ):
            # Handle User input of number
            number_input = 0
            if operation != "q" and operation != "r":
                number_input = input("Enter another number: ")

            # Check that input number is int or float
            if calculator.is_float(number=number_input) or calculator.is_int(
                number=number_input
            ):

                # Convert input string to float numbers
                calculated_sum = float(calculated_sum)
                number_input = float(number_input)

                # Set the new sum
                new_sum = 0.0

                # Switch case based on selected operation
                match operation:
                    case "q":
                        quit_calc = True
                    case "r":
                        calculated_sum = input("Calculator restarted. Enter a number: ")
                        if calculated_sum == "q":
                            quit_calc = True
                        else:
                            restart_calc = True
                    case "+":  # Add
                        new_sum = calculator.add(calculated_sum, number_input)
                    case "-":  # Subtract
                        new_sum = calculator.subtract(calculated_sum, number_input)
                    case "/":  # Divide
                        new_sum = calculator.divide(calculated_sum, number_input)
                        if new_sum is None:
                            print("Division by zero is not allowed. Shutting down!")
                            quit_calc = True

                    case "*":  # Multiply
                        new_sum = calculator.multiply(calculated_sum, number_input)

                # Print the result if operation is not to restart/quit the calculator
                if restart_calc is False and quit_calc is False:
                    print(f"{calculated_sum} {operation} {number_input} = {new_sum}")
                    calculated_sum = new_sum

            else:
                print("Not a valid number! Try again.")
        else:
            print("Not a valid operation! Try again.")


if __name__ == "__main__":
    main()
