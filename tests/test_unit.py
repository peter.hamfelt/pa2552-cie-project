import calculator

""" PETER @peter.hamfelt """


def test_divide_with_assert_given_int():
    """Given"""
    negative_number = -1337
    number = 1337
    zero = 0
    test = 10

    """ When """
    negative_number_result = calculator.divide(x=negative_number, y=negative_number)
    number_result = calculator.divide(x=number, y=number)
    zero_result = calculator.divide(x=zero, y=zero)
    test_result = calculator.divide(x=test, y=test)

    """ Then """
    assert test_result == 1
    assert negative_number_result == 1
    assert number_result == 1
    if zero_result:
        assert None


def test_divide_with_assert_given_float():
    """Given"""
    negative_number = -1337.000
    number = 1337.000
    zero = 0.000

    """ When """
    negative_number_result = calculator.divide(x=negative_number, y=negative_number)
    number_result = calculator.divide(x=number, y=number)
    zero_result = calculator.divide(x=zero, y=zero)

    """ Then """
    assert negative_number_result == 1.000
    assert number_result == 1.000
    if zero_result:
        assert None


def test_divide_with_division_zero():
    """Given"""
    number = 420.000
    zero = 0

    """ When """
    number_result_0 = calculator.divide(x=number, y=zero)
    number_result_1 = calculator.divide(x=zero, y=number)
    zero_result = calculator.divide(x=zero, y=zero)

    """ Then """
    # Division by zero
    if number_result_0:
        assert None

    assert number_result_1 == 0

    # Division by zero
    if zero_result is None:
        assert True


def test_check_instance_with_numbers():
    """Given"""
    neg_number = -5
    neg_number_float = -5.5
    number = 10
    number_float = 10.10
    zero = 0
    zero_float = 0.0

    """ When """
    result_0 = check_instance(calculator.divide(x=neg_number, y=number))
    result_1 = check_instance(calculator.divide(x=neg_number_float, y=number_float))
    result_2 = check_instance(calculator.divide(x=zero, y=neg_number_float))
    result_3 = check_instance(calculator.divide(x=zero_float, y=number_float))
    result_4 = check_instance(calculator.divide(x=zero, y=number_float))
    result_5 = check_instance(calculator.divide(x=zero_float, y=neg_number))
    result_6 = check_instance(calculator.divide(x=number, y=zero))

    """ Then """
    results = [result_0, result_1, result_2, result_3, result_4, result_5, result_6]
    for i in results:
        if i is True:
            assert True
        elif i is False:
            assert True


def test_check_instance_with_strings():
    """Given"""
    str_number_float = "2.0"
    str_number = "3"
    str_text = "four"
    str_double_minus_sign = "--2"

    """ When """
    result_0 = check_instance(calculator.divide(x=str_number_float, y=str_number))
    result_1 = check_instance(calculator.divide(x=str_double_minus_sign, y=str_text))
    result_2 = check_instance(calculator.divide(x=str_number, y=str_text))
    result_3 = check_instance(
        calculator.divide(x=str_double_minus_sign, y=str_number_float)
    )

    """ Then """
    results = [result_0, result_1, result_2, result_3]
    for i in results:
        if i is False:
            assert True


def check_instance(calc):
    if calculator.is_float(number=calc):
        return True
    elif calculator.is_int(number=calc):
        return True
    else:
        return False


""" Muhammad Umar @muur21 """


def test_subtraction():
    assert 0 == calculator.subtract(x=5, y=5)
    assert 2.5 == calculator.subtract(5.5, 3)
    assert 8 == calculator.subtract(9, 1)
    assert -9 == calculator.subtract(90, 99)
    assert -10 == calculator.subtract(-5, 5)
    assert 0 == calculator.subtract(-5, -5)


""" Sri Harsha Namburu @horribleharsha """


def test_multiply_with_float():
    assert 25.000 == calculator.multiply(x=5.000, y=5.000)


def test_multiply_with_zero():
    assert 0 == calculator.multiply(x=10, y=0)


def test_multiply_with_int():
    assert 100 == calculator.multiply(x=20, y=5)


""" Usman Ahmad @ahmadusman.se """


def test_addition():
    assert 10 == calculator.add(x=5, y=5)


def test_addition_assert_given_float():
    assert 5.6 == calculator.add(x=3.1, y=2.5)


def test_addition_assert_with_one_negative_int():
    assert 3 == calculator.add(x=5, y=-2)


def test_addition_assert_with_one_zero():
    assert 5 == calculator.add(x=5, y=0)


def test_addition_assert_with_two_negative_int():
    assert -7 == calculator.add(x=-5, y=-2)
