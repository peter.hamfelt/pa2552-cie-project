from calculator import add, multiply, divide, subtract
from main import main
from unittest.mock import MagicMock

""" PETER @peter.hamfelt """


def test_basic_calculator_operations():
    """Given"""
    five = 5
    two = 2
    fifty_five = 55
    fifty = 50
    total_sum = 0

    """ When """
    total_sum = add(x=total_sum, y=five)
    total_sum = subtract(x=fifty_five, y=total_sum)
    total_sum = multiply(x=total_sum, y=two)
    divide(x=total_sum, y=fifty)

    """ Then """
    assert total_sum == 100


def test_calculator_menu_operations(monkeypatch):
    """Given"""
    user_inputs = iter(
        [
            "10",
            "Incorrect",
            "/",
            "10.",
            "+",
            "99",
            "-",
            "50",
            "*",
            2.0,
            "/",
            -100,
            "r",
            "q",
        ]
    )

    """ When """
    monkeypatch.setattr("builtins.input", lambda name: next(user_inputs))

    """ Then """
    # Run calculator menu
    if main():
        assert True


""" Muhammad Umar @muur21 """


def test_basic_calculator_subtract():
    total_sub = 100
    total_sub = add(x=total_sub, y=5)
    total_sub = multiply(x=total_sub, y=2)
    total_sub = divide(x=total_sub, y=50)
    total_sub = subtract(x=10, y=total_sub)

    assert total_sub == 5.8


""" Usman Ahmad @ahmadusman.se """


def test_basic_calculator_addition():
    assumed_value = 0
    assumed_value = add(x=assumed_value, y=20)
    assumed_value = subtract(x=100, y=assumed_value)
    assumed_value = multiply(x=assumed_value, y=3)
    assumed_value = divide(x=assumed_value, y=30)
    assert assumed_value == 8
