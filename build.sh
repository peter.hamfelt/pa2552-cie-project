#!/bin/bash

echo Checking Prerequisites...

echo Installed Python version
py --version

echo Project Setup...

echo Install virtual environment
py -m venv venv
source venv/Scripts/activate

echo Install requirements
pip install -r requirements.txt
pre-commit install
pre-commit autoupdate

echo Running tests...
pytest -s -v --color=yes --cov

echo Running Calculator...
py main.py