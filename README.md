# PA2552-CIE-Project

## Getting started

### Development

1. Install python (download from web)
2. Install git (download from web)
3. Preferable IDE: PyCharm community 
4. create virtual env for system requirements (follow commands below)
5. Start Developing! 

```
$ cd PA2552-CIE-Project
$ py -m venv venv
### Activate venv ###
$ source venv/scripts/activate
### Install requirements ###
$ pip install -r requirements.txt
$ pre-commit install
$ pre-commit autoupdate
```

#### Run Test
Make sure to have your venv activated `$ source venv/scripts/activate`
````
$ pytest -s -v --color=yes --cov
````

#### Run Pre-commit Checkers
````
$ pre-commit run --all-files
````

#### Run Calculator
````
$ py main.py
````

#### Push to Git
Follow the steps below for pushing your code to git
````
# 0. View and make sure you have all changes and also make sure you have the latests changes from remote repo
$ git pull origin main
$ git status 

# 1. Add your changes for commit
$ git add .

# 2. Create your commit
$ git commit -m "Write here what you have done/changed"
Hold your horses, did BLACK or FLAKE8 complain and not pass? 
No worries just perform step 1 and 2 again and all should be ok.

# 3. Push your code to remote repo
$ git push origin <your branch name here>

Well done!
````

## Run Calculator (automated build)
Open up a git bash terminal in the project root folder _($ cd PA2552-CIE-Project)_ then run:
````
$ ./build.sh
````

Enjoy the calculator! 