"""
Calculator performing basic math operations.
"""

""" Peter Hamfelt @peter.hamfelt """


def divide(x, y):
    # Make sure the numbers are of float
    if is_float(x) and is_float(y):
        # Division of zero
        if float(y) == float(0):
            return None
        else:
            total_sum = float(x) / float(y)
            return total_sum
    # Return None if numbers are not float
    else:
        return None


def is_float(number):
    try:
        float(number)
    except Exception:
        return False
    else:
        return True


def is_int(number):
    try:
        int(number)
    except Exception:
        return False
    else:
        return True


""" Muhammad Umar @muur21 """


def subtract(x, y):
    if is_float(x) and is_float(y):
        total_sum = float(x) - float(y)
        return total_sum
    else:
        return None


""" Sri Harsha Namburu @horribleharsha """


def multiply(x, y):
    if is_float(x) and is_float(y):
        total_sum = float(x) * float(y)
        return total_sum
    else:
        return None


""" Usman Ahmad @ahmadusman.se """


def add(x, y):
    if is_float(x) and is_float(y):
        total_sum = float(x) + float(y)
        return total_sum
    # Return None if numbers are not float
    else:
        return None
